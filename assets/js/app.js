import axios from 'axios';
import { User } from "./user";
import { createIcons } from "./icons";

const persons = [];

function init(){
    const buttonAddpersons = document.querySelector("#button");
    console.log(buttonAddpersons);
    buttonAddpersons.addEventListener('click', handleCallApiForCreatePersons);

    const buttonBlockAddpersons = document.querySelector("#button-block");
    buttonBlockAddpersons.addEventListener('click', handleCallApiForCreatePersons);
    createIcons();
};

async function handleCallApiForCreatePersons(){
    const response = await axios.get("https://randomuser.me/api/");
    const currentData = response.data.results[0];
    const USER = new User(
        currentData.name.first,
        currentData.name.last,
        currentData.dob.age,
        currentData.location.street.number,
        currentData.location.street.name,
        currentData.location.postcode,
        currentData.location.city,
        currentData.email,
        currentData.picture.large);

    persons.push(USER);
    console.log(USER);

    //createHtmlForUser();
    USER.createHtmlUserDisplay(persons);
    createIcons();
};

init();
