export class User {

    constructor(firstname, lastname, age,numberStreet, nameStreet, postcode, city,mail, picturePath){
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.numberStreet = numberStreet;
        this.nameStreet = nameStreet;
        this.postcode = postcode;
        this.city = city;
        this.mail = mail;
        this.picturePath = picturePath;
    }

    get fullname(){
       return this.firstname + " " + this.lastname;
    }

    get address(){
        return this.numberStreet + " " + this.nameStreet + "<br>"
               + this.postcode + " " + this.city;
    }

    createHtmlUserDisplay(persons){
        const divPersonElement = document.querySelector("#persons");

        let html ;
        for(let index in persons){
            html =
            `<div class="person">` +
                `<img src="${persons[index].picturePath}" alt="photo de profil" class="person__image"/>` +
                `<div class="person__info">` +
                    `<h2 class="person__info-fullname"> ${persons[index].fullname} </h2>` +
                    `<p class="person__info-age">
                        <i icon-name="gift"></i>
                         Age : ${persons[index].age}
                    </p>` +
                    `<p class="person__info-address">
                        <i icon-name="home"></i>
                         ${persons[index].address}
                    </p>` +
                    `<p class="person__info-mail">
                        <i icon-name="mail"></i>
                         ${persons[index].mail}
                    </p>` +
                `</div>` +
            `</div>`;
        }

        divPersonElement.insertAdjacentHTML("afterbegin", html);
    }
}