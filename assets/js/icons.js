// Recommended way, to include only the icons you need.
import { createIcons, Home, UserPlus, Mail, Gift } from 'lucide';

export function createIcons(){
  createIcons({
      icons: {
        Home,
        UserPlus,
        Mail,
        Gift
      },
      attrs: {
        class: ['icon'],
        'stroke-width': 0.5,
        stroke: '#333',
      },
  });
};